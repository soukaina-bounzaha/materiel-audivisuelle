package org.sid.audivisuelle.dao;

import org.sid.audivisuelle.entities.Personne;

public interface IPersonnelDao extends IGenericDao<Personne>{

}
