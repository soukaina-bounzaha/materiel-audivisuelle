package org.sid.audivisuelle.dao;

import java.util.Collection;

public interface IGenericDao<E> {
 
	public E save(E entity);
	
	public E update(E entity);
	
	public Collection<E> selectAll();
	
	public Collection<E> selectAll(String sortField, String sort);
	
	public E getById(Long id);
	
	public void remove(Long id);
	
	public E findOne(String paramName, Object paramValue);
	
	public E findOne(String[] paramNames, Object[] paramValues);
	
	public int findCountBy(String paramName, String paramValue);
}
