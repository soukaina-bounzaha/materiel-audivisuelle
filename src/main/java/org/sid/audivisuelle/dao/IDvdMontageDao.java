package org.sid.audivisuelle.dao;

import org.sid.audivisuelle.entities.DvdMontage;

public interface IDvdMontageDao extends IGenericDao<DvdMontage>{

}
