package org.sid.audivisuelle.dao.Impl;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Collection;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.sid.audivisuelle.dao.IGenericDao;

@SuppressWarnings("unchecked")

public class GenericDaoImpl<E> implements IGenericDao<E> {
	
	@PersistenceContext  // definr entitymanager
	EntityManager em;
	
	private Class<E> type ;
	
	public Class<E> getType() {
		return type;
	}

	public GenericDaoImpl() {
		Type t = getClass().getGenericSuperclass();
		ParameterizedType pt = (ParameterizedType) t;
		type = (Class<E>) pt.getActualTypeArguments()[0];
		
	}

	@Override

	public E save(E entity) {
		em.persist(entity);
		return entity;
	}

	@Override
	public E update(E entity) {
		return em.merge(entity);
	}

	@Override
	public Collection<E> selectAll() {
		Query query = em.createQuery("select t from "+ type.getSimpleName()+ "t");  // select * from nom-table
		return query.getResultList();
	}

	@Override
	public Collection<E> selectAll(String sortField, String sort) {
		Query query = em.createQuery("select t from "+ type.getSimpleName()+ "t order by" + sortField + "" + sort);  // select * from nom-table order by nom desc ou asc
		return query.getResultList();
	}

	@Override
	public E getById(Long id) {
	  return em.find(type, id);
		
	}

	@Override
	public void remove(Long id) {
		E tab = em.getReference(type, id); // recuperer la ref de la table
		em.remove(tab);
	}

	@Override
	public E findOne(String paramName, Object paramValue) {
		Query query = em.createQuery("select t from "+ type.getSimpleName()+ "t where " +paramName+ " =:x" ); // c du jpql
		query.setParameter(paramName, paramValue);
		return query.getResultList().size() > 0 ? (E) query.getResultList().get(0) : null ;
		
	}

	@Override
	public E findOne(String[] paramNames, Object[] paramValues) {
		if (paramNames.length != paramValues.length) {
			return null; 
		}
	String queryString = "select e from " + type.getSimpleName() + "e where ";
	int len = paramNames.length;
	for (int i =0; i< len;i++) {
		queryString += "e." + paramNames[i] + " = :x" +i ;
		if ((i+1) < len) {
			queryString+= "and";	
		}
  	}
	Query query = em.createQuery(queryString);
	for (int i =0; i< paramValues.length;i++) {
		query.setParameter("x" + i ,paramValues[i]);
	}
	return query.getResultList().size() > 0 ? (E) query.getResultList().get(0) : null ;
	}	

	@Override
	public int findCountBy(String paramName, String paramValue) {
		Query query = em.createQuery("select t from "+ type.getSimpleName()+ "t where " +paramName+ " =:x" ); // c du jpql
		query.setParameter(paramName, paramValue);
		return query.getResultList().size() > 0 ? ((Long) query.getSingleResult()).intValue() : 0 ;
		
	}

	
}
