package org.sid.audivisuelle.dao;

import org.sid.audivisuelle.entities.Rush;

public interface IRushDao extends IGenericDao<Rush> {

}
