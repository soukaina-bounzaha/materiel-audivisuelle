package org.sid.audivisuelle.dao;

import org.sid.audivisuelle.entities.Utilisateur;

public interface IUtilisateurDao extends IGenericDao<Utilisateur> {

}
