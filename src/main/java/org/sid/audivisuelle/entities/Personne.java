package org.sid.audivisuelle.entities;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;



import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
@Entity
@Data @AllArgsConstructor  @NoArgsConstructor @ToString 
public class Personne implements Serializable{
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	 private Long idPersonne;
	@Column( length=25)
	 private String nomPersonne;
	@Column( length=25)
	 private String prenomPersonne;
	 private String photo;
	 @Column( length=25)
	 private String grade;
	 @Column( length=25)
	 private String ville;
	 @Column( length=25)
	 private String martricule;
	 @Column( length=25)
	 private String situationfamille;
	 @Column( length=25)
	 private String nomEpouse;
	 @Column( length=25)
	 private String prenomEpouse ;
	 @Column( length=25)
	 private String professionEpouse;
	 @Column( length=25)
	 private String OrganismeEmployeur;
	 @Column( length=25)
	 private String adressepersonnelle;
	 private int gsm ;
	 private int fix;
	 @Column( length=25)
	 private String nomPersonneAccident;
	 @Column( length=25)
	 private String adressePersonneAccident;
	 @Column( length=25)
	 private String telephonePersonneAccident;
	 @OneToMany(mappedBy="personne")
	 private Collection<RushPersonne> rushpersonnes;

}
