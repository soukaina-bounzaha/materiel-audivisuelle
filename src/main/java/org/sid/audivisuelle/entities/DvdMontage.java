package org.sid.audivisuelle.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
@Entity
@Data @AllArgsConstructor  @NoArgsConstructor @ToString 
public class DvdMontage implements Serializable {
	
		@Id @GeneratedValue(strategy = GenerationType.IDENTITY) 
		 private Long idDvdMontage;
		@Column( length=25)
		 private String nomMonteur;
		@Column( length=25)
		 private String PrenomMonteur;
		 @Temporal(TemporalType.TIMESTAMP)
		  private Date dureeMontage;
		 @Temporal(TemporalType.DATE)
		  private Date dateMontage;
		 @Column( length=75)
		  private String DesigDvd;
		  private int numeroDvd;
		  @Column( length=25)
		  private String lienDvd;
	 
	 @ManyToOne
	private Evenement evenement ;
	 @ManyToOne
		private Rayon rayon;
	  

}
