package org.sid.audivisuelle.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
@Entity
@Data @AllArgsConstructor @NoArgsConstructor @ToString

public class Utilisateur implements Serializable{
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idUtilisateur;
	@Column( length=25)
	private String nomUtilistauer;
	@Column( length=25)
	private String prenomUtilisateur;
	@Column( length=25)
	private String Mdp;
	private String photo;
	

}
