package org.sid.audivisuelle.entities;

import java.io.Serializable;

import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
@Entity
@Data @AllArgsConstructor  @NoArgsConstructor @ToString 
public class RushPersonne implements Serializable {
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	 private Long idRushPersonne;
	 private int numeroRushPersonne;
	 @ManyToOne
	private Rush rush;
	 @ManyToOne
	private Personne personne; 
	

}
