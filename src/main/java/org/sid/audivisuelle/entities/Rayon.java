package org.sid.audivisuelle.entities;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
@Entity
@Data @AllArgsConstructor  @NoArgsConstructor @ToString 
public class Rayon implements Serializable {
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idRayon;
	@Column( length=25)
	private String nomRayon;
	private int numColone;
	private int numRange;
	
	@OneToMany(mappedBy="rayon")
	private Collection<Cassette> cassettes ;
	@OneToMany(mappedBy="rayon")
	private Collection<DvdMontage> dvdmontages ; 

}
