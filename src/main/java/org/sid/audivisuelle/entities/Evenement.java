package org.sid.audivisuelle.entities;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Data @AllArgsConstructor  @NoArgsConstructor @ToString 

public class Evenement implements Serializable{
     @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idEven;
     @Column( length=75)
	private String DesigEven;
     @Column( length=25)
	private String refereven;
	@ManyToOne
	private Categorie categorie;
	@OneToMany(mappedBy="evenement")
	private Collection<Rush> rushs;
	@OneToMany(mappedBy="evenement")
	private Collection<DvdMontage> dvdMontages;

	
}
