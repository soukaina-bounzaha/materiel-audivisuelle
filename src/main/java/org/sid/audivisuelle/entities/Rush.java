package org.sid.audivisuelle.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
@Entity
@Data @AllArgsConstructor  @NoArgsConstructor @ToString 
public class Rush implements Serializable{
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idRush;
	@Column( length=75)
	private String desigRush;
	 @Temporal(TemporalType.TIMESTAMP)
	 private Date dureeRush;
	 @Temporal(TemporalType.DATE)
	  private Date dateRush;
	 @Column( length=25)
	private String villeRush;
	 @Column( length=25)
	private String uniteRush;
	private int NumeroRush;
	 @ManyToOne
	private Evenement evenement ;
	 @OneToMany(mappedBy= "rush")
	 private Collection<RushPersonne> rushpersonnes;
	 @OneToMany(mappedBy= "rush")
	 private Collection<Cassette> cassettes;
	  
	
	

}
