package org.sid.audivisuelle.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
@Entity
@Data @AllArgsConstructor  @NoArgsConstructor @ToString 
public class Cassette implements Serializable{
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idCassette;
	@Column( length=25)
	private String desigCassette;
	@Temporal(TemporalType.TIMESTAMP)
	private Date dureeCassette;
	@Column( length=75)
	private String LienCassette;
	private int NumeroCassette;
	@ManyToOne
	private Rush rush;
	@ManyToOne
	private Rayon rayon;
	
	

}
