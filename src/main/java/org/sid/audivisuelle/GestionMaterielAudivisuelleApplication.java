package org.sid.audivisuelle;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GestionMaterielAudivisuelleApplication {

	public static void main(String[] args) {
		SpringApplication.run(GestionMaterielAudivisuelleApplication.class, args);
	}

}
